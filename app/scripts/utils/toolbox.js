/**
 * Created by three on 14-7-24.
 */


/**
 * 对象字符串替换
 * @param T
 * @param O
 * @returns {*}
 */
function obj2html(T, O) {
    var html = T;
    for(var k in O) {
        var reg=new RegExp("{"+k+"}","g");
        html = html.replace(reg, O[k]);
    }

    return html;
}

/**
 * js加载
 * @type {{}}
 */
var jsArray = {};
function loadScript(c, d) {
    if (!jsArray[c]) {
        jsArray[c] = true;
        var a = document.getElementsByTagName("body")[0];
        var b = document.createElement("script");
        b.type = "text/javascript";
        b.src = c;
        b.onload = d;
        a.appendChild(b)
    } else {
        if (d) {
            return d()
        }
    }
}

/**
 * 日期格式化
 */
Date.prototype.format = function (format){
    var date = this;
    var paddNum = function(num){
        num += "";
        return num.replace(/^(\d)$/,"0$1");
    }
    //指定格式字符
    var cfg = {
        yyyy : date.getFullYear() //年 : 4位
        ,yy : date.getFullYear().toString().substring(2)//年 : 2位
        ,M  : date.getMonth() + 1  //月 : 如果1位的时候不补0
        ,MM : paddNum(date.getMonth() + 1) //月 : 如果1位的时候补0
        ,d  : date.getDate()   //日 : 如果1位的时候不补0
        ,dd : paddNum(date.getDate())//日 : 如果1位的时候补0
        ,hh : date.getHours()  //时
        ,mm : date.getMinutes() //分
        ,ss : date.getSeconds() //秒
    }
    format || (format = "yyyy-MM-dd hh:mm:ss");
    return format.replace(/([a-z])(\1)*/ig,function(m){return cfg[m];});
};
function formatDate(date,format){
    var paddNum = function(num){
        num += "";
        return num.replace(/^(\d)$/,"0$1");
    }
    //指定格式字符
    var cfg = {
        yyyy : date.getFullYear() //年 : 4位
        ,yy : date.getFullYear().toString().substring(2)//年 : 2位
        ,M  : date.getMonth() + 1  //月 : 如果1位的时候不补0
        ,MM : paddNum(date.getMonth() + 1) //月 : 如果1位的时候补0
        ,d  : date.getDate()   //日 : 如果1位的时候不补0
        ,dd : paddNum(date.getDate())//日 : 如果1位的时候补0
        ,hh : date.getHours()  //时
        ,mm : date.getMinutes() //分
        ,ss : date.getSeconds() //秒
    }
    format || (format = "yyyy-MM-dd hh:mm:ss");
    return format.replace(/([a-z])(\1)*/ig,function(m){return cfg[m];});
}