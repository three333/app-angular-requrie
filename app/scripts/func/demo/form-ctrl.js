/**
 * Created by three on 14-9-29.
 */
define(['angular','ng/ng.socketio','plugin/upload/angular-file-upload'], function (angular) {
    'use strict';
    angular.module('app.modules.demo.form', ['app.func.common.service', 'app.socket', 'angularFileUpload'])
        .controller('FormCtrl', ['$scope', '$debounce', 'FileUploader', 'Config', function ($scope, $debounce, FileUploader, Config) {

        }])
    ;
});