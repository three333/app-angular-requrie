/**
 * Created by three on 14-10-9.
 */
define(['angular','stomp-websocket','ng/ng.socketio','plugin/upload/angular-file-upload'], function (angular) {
    'use strict';
    angular.module('app.modules.demo.sockjs.stomp', ['app.func.common.service', 'app.socket', 'angularFileUpload'])
        .controller('SockjsStompCtrl', ['$scope', '$debounce', 'FileUploader', 'Config', function ($scope, $debounce, FileUploader, Config) {
//            var socket = new WebSocket('ws://127.0.0.1:8080/connect');
            $scope.echoList = [];
            var options = {
                protocols_whitelist: [
                    "websocket",
                    "xhr-streaming",
                    "xdr-streaming",
                    "xhr-polling",
                    "xdr-polling",
                    "iframe-htmlfile",
                    "iframe-eventsource",
                    "iframe-xhr-polling"
                ],
                debug: true
            };
            var socket = new SockJS('http://127.0.0.1:8080/ws/connect', "aaa", options);
            var stompClient = Stomp.over(socket);
            var connectHeader = {
                user: 'aaa',
                login: 'aaa',
                passcode: 'aaa',
                // additional header
                'client-id': 'my-client-id'
            };
            stompClient.connect(/*"aaa", "aaa"*/{}, function(frame) {
                // errors
                stompClient.subscribe('/user/queue/errors', function () {
                    console.log(arguments)
                })
                //test
                stompClient.subscribe('/topic/greetings', function(greeting){
                    console.log(greeting.body);
                });

                // 订阅版本信息
                stompClient.subscribe('/echo/broadcast', function (echo) {
                    $scope.$apply(function () {
                        $scope.echoList.push(JSON.parse(echo.body));
                    });
                });
                // 订阅系统消息
                stompClient.subscribe('/topic/system/msg', function (echo) {
                    $scope.$apply(function () {
                        $scope.echoList.push(JSON.parse(echo.body));
                    });
                });
                // 订阅房间初始化信息
                // 用户加入房间操作
                stompClient.subscribe("/app/join/joinRoom", function (echo) {
                    $scope.$apply(function () {
                        $scope.echoList.push(JSON.parse(echo.body));
                    });
                });
                // 订阅房间信息广播
                stompClient.subscribe("/topic/broadcast/joinRoom", function (echo) {
                    $scope.$apply(function () {
                        $scope.echoList.push(JSON.parse(echo.body));
                    });
                });
                // 订阅用户发送房间信息的返回结果
                stompClient.subscribe("/user/queue/send/joinRoom", function(echo) {
                    console.log(arguments);
                    $scope.$apply(function () {
                        $scope.echoList.push(JSON.parse(echo.body));
                    });
                });

                // 订阅房间内特定用户发送消息
                // convertAndSendToUser 没有生效
                stompClient.subscribe("/user/queue/recv/joinRoom", function(echo) {
                    $scope.$apply(function () {
                        $scope.echoList.push(JSON.parse(echo.body));
                    });
                });

            },function(error) {
                console.log(error);
            });

            $scope.sendEcho = function () {
                stompClient.send("/app/echo", {}, JSON.stringify({ 'content': $scope.echo }));
            }

            // 用户发送房间消息
            $scope.joinRoom = function () {
                stompClient.send("/app/send/joinRoom", {}, JSON.stringify({ 'content': $scope.echo, type:"join" }));
            }
        }])
    ;
});
