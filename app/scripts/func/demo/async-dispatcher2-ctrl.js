/**
 * Created by three on 14-10-15.
 */
define(['app'], function (app) {
    'use strict';

    console.log(app);

    var injectParams = ['$scope'];
    var controller = function ($scope) {
        $scope.aa = "22222222";

        console.log($scope.aa);
    };

    controller.$inject = injectParams;

    app.register.controller('AsyncDispatcher2Ctrl', controller);
});