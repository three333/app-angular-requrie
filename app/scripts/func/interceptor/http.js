define(['angular','utils/base64', 'func/base/config'], function (angular) {
    'use strict';
    /**
     * Created by three on 14-7-15.
     */

    angular.module('app.func.interceptor.http', [
        'app.func.config'
    ])
        .factory('TokenInterceptor', ["$q", "$window", 'AuthenticationService', function ($q, $window, AuthenticationService) {
            return {
                request: function (config) {

                    config.headers = config.headers || {};
//                    http basic auth 认真需要
//                    if ($window.sessionStorage.token) {
//                        config.headers.Authorization = AuthenticationService.authType+ ' ' + $window.sessionStorage.token;
//                    }
                    return config;
                },
                response: function(response) {
                    return response;
                }
            };
        }])
        .factory('TimestampMarker', [function() {
            var timestampMarker = {
                request: function(config) {
                    config.requestTimestamp = new Date().getTime();
                    return config;
                },
                response: function(response) {
                    response.config.responseTimestamp = new Date().getTime();
                    return response;
                }
            };
            return timestampMarker;
        }])
//        .factory('sessionInjector', ['SessionService', function(SessionService) {
//            var sessionInjector = {
//                request: function(config) {
//                    if (!SessionService.isAnonymus) {
//                        config.headers['x-session-token'] = SessionService.token;
//                    }
//                    return config;
//                }
//            };
//            return sessionInjector;
//        }])
        .factory('sessionRecoverer', ['$q', '$injector', function($q, $injector) {
            var sessionRecoverer = {
                responseError: function(response) {
                    // Session has expired
                    if (response.status == 419){
                        var SessionService = $injector.get('SessionService');
                        var $http = $injector.get('$http');
                        var deferred = $q.defer();

                        // Create a new session (recover the session)
                        // We use login method that logs the user in using the current credentials and
                        // returns a promise
                        SessionService.login().then(deferred.resolve, deferred.reject);

                        // When the session recovered, make the same backend call again and chain the request
                        return deferred.promise.then(function() {
                            return $http(response.config);
                        });
                    }
                    return $q.reject(response);
                }
            };
            return sessionRecoverer;
        }])
        .factory('HttpErrorInterceptor', ["$q", "$window","$rootScope","HttpErrorHandlerConfig", function ($q, $window, $rootScope, HttpErrorHandlerConfig) {
            return {
                // optional method
                'requestError': function(rejection) {
                    console.log("请求错误");
                    console.log(rejection)
                    // do something on error
//                    if (canRecover(rejection)) {
//                        return responseOrNewPromise
//                    }
                    return $q.reject(rejection);
                },

                // optional method
                'responseError': function(response) {
                    console.log("请求返回错误");
                    var res = HttpErrorHandlerConfig.error(response.status,response);
                    if(res!=null) {
                        return res;
                    } else {
                        return $q.reject(response);
                    }
                }
            };
        }])
    ;
});