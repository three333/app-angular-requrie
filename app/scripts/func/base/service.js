define(['angular','utils/base64'], function (angular) {
    'use strict';
    /**
     * Created by three on 14-7-15.
     */

    var data_serve_base_url = 'http://127.0.0.1:8080';
    var dataServiceDef = [
        {name:'FunctionService',        url:data_serve_base_url+'/system/func/:id',         type:'restful'},
        {name:'UserFuncService',        url:data_serve_base_url+'/user/menu/enable',        type:'restful'},
        {name:'UserService',            url:data_serve_base_url+'/user/:id',                type:'restful'},
        {name:'ConfigConstantService',  url:data_serve_base_url+'/config/constant',         type:'restful'}
    ];
    var defaultData = {id:'@id'};
    var defaultMethods = {
        update: {method: 'PUT', params: {}, isArray: false},
        add: {method: 'POST', params: {}, isArray: false},
        list: {method: 'GET', params: {}, isArray: true},
        detail: {method: 'GET', params: {}, isArray: false},
        delete: {method: 'DELETE', params: {}, isArray: false}
    };
    var service = angular.module('app.func.common.service', []).run([function () { }]);
    (function (dataServiceDef) {

        for(var i=0; i<dataServiceDef.length; i++) {
            var  serviceDef = dataServiceDef[i];

            (function (serviceDef) {
                angular.module('app.func.common.service').factory(serviceDef.name,['$resource', '$http', function ($resource, $http) {

                    if(serviceDef.type=='restful') { // restful

                        var data = serviceDef.data || defaultData;

                        // 配置请求方法
                        var realMethods = JSON.parse(JSON.stringify(defaultMethods));
                        for(var method in serviceDef.methods) {
                            realMethods[method] = serviceDef.methods[method];
                        }

                        return $resource(serviceDef.url, data, realMethods);

                    } else if (serviceDef.type=='http') {
                        var serv = {};
                        for(var method in serviceDef.methods) {
                            var methodConf = serviceDef.methods[method];
                            serv[method] = function (data, suc_cb, err_cb) {

                                $http({
                                    method: methodConf.method,
                                    url: serviceDef.url+(methodConf.url || ''),
                                    params:{},
                                    data:data
                                }).
                                    success(function(data, status, headers, config) {
                                        suc_cb(data, status, headers, config);
                                    }).
                                    error(function(data, status, headers, config) {
                                        err_cb(data, status, headers, config);
                                    });
                            };
                        }
                        return serv;
                    }
                }])
            })(serviceDef);
        }
    })(dataServiceDef);


    service.factory('AuthenticationService', ["$window",function ($window) {
        var loginUser = null;
        var userSession = null;
        var auth = {
            userSession:function(session) {
                if(session) {
                    userSession = session;
                    $window.sessionStorage.userSession = session;
                }
                return userSession;
            },
            loginUser: function (user) {
                loginUser = user;
                $window.sessionStorage.loginUser = user==null?null:JSON.stringify(loginUser)
            },
            currentUser: function () {
                return loginUser;
            },
            isLogin: false,
            make_basic_auth:function (user, pwd) {
                var tok = user + ':' + pwd;
                var hash = Base64.encode(tok);
                return hash;
            },
            authType:'Basic',
            createAuth: function () {
                return this.authType+ ' ' + $window.sessionStorage.token;
            },
            logout: function () {
                this.isLogin = false;
                $window.sessionStorage.token = null;
                $window.sessionStorage.loginUser = null;
                $window.sessionStorage.userSession = null;
            }
        };
        (function () {
            auth.isLogin = $window.sessionStorage.token!=null?true:false;
            if($window.sessionStorage.loginUser) {
                auth.loginUser(JSON.parse($window.sessionStorage.loginUser));
            } else {
                auth.loginUser(null);
            }
            if($window.sessionStorage.userSession) {
                auth.userSession($window.sessionStorage.userSession);
            } else {
                auth.userSession(null);
            }
        })();
        return auth;
    }])
    ;

});