define(['angular', 'func/base/config', 'ng/ng.plugins'], function (angular) {
    'use strict';

    /**
     * @ngdoc function
     * @name appApp.controller:FuncUserLoginCtrl
     * @description
     * # FuncUserLoginCtrl
     * Controller of the appApp
     */
    angular.module('app.func.user', [
        'app.func.config',
        'app.plugins'
    ])
    .controller('UserManagerCtrl', ['$log','$compile','$scope', '$location','$debounce','$window','$modal','UserService','Config','AuthenticationService', function ($log,$compile,$scope, $location,$debounce,$window,$modal, UserService,Config,auth) {
        $scope.userTable = null; // 存放 datatable对象
        /**
         * 配置datatable参数
         * @returns {*}
         */
        $scope.getTableConfig = function () {

            return $scope.getAuthTableConfig(Config.data_serve_base_url+'/user', {
                aoColumns: [
                    { "sTitle": "用户名", "mDataProp": "name" },
                    { "sTitle": "用户密码", "mDataProp": "pwd" },
                    { "sTitle": "管理",
                        "sClass": "center",
                        "bSortable": false,
                        "mDataProp": null,
                        "mData":null,
                        "render": $scope.manageColumnT2
                    }
                ],
                fnServerParams: function ( aoData ) {
                    if($scope.user) {
                        for(var k in $scope.user) {
                            aoData[k] = $scope.user[k]
                        }
                    }
                }
            });

        };
        $scope.user = {};
        $scope.search = function () { // 搜索
            $scope.userTable.fnPageChange(0, true);
        };

        $scope.delete = function (id) {
            $scope.$emit(Config.events.loading, { type: 'loading' })
            var result = UserService.delete({id:id}, function () {
                console.log(result);
                $scope.search();
                $scope.$emit(Config.events.loading, {
                    type: 'loaded'
                })
                $scope.$emit(Config.events.showMsg, {
                    type: 'success',
                    title: 'success',
                    content: result.code+" : "+result.msg,
                    config:{},
                    callback: function () { }
                })
            })
        };
        $scope.edit = function (id) {
            $scope.$emit(Config.events.loading, { type: 'loading' })
            $scope.title = "修改用户";
            var data = UserService.get({id:id}, function () {
                $scope.$emit(Config.events.loading, { type: 'loaded' })
                $scope.selectedUser = data.result;
                var modalInstance = $modal.open({
                    templateUrl: 'scripts/func/user/template/add.html',
                    controller: function ($scope, $modalInstance, title, user) {

                        $scope.title = title;
                        $scope.user = {};
                        for(var k in user) {
                            $scope.user[k] = user[k]
                        }


                        $scope.ok = function () {
                            $scope.$emit(Config.events.loading, {
                                type: 'loading'
                            })
                            console.log($scope.user)
                            var result = UserService.update($scope.user, function () {
                                $modalInstance.close(result);
                                $scope.$emit(Config.events.loading, {
                                    type: 'loaded'
                                });
                            })

                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                        $scope.reset = function () {
                            $scope.user = {};
                            for(var k in user) {
                                $scope.user[k] = user[k]
                            }
                        }
                    },
                    size: null, // 'lg','sm'
                    resolve: {
                        title: function () {
                            return $scope.title;
                        },
                        user: function () {
                            return $scope.selectedUser;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    $log.info(result);
                    $scope.search();
                    $scope.$emit(Config.events.showMsg, {
                        type: 'success',
                        title: '操作成功',
                        content: '获取数据成功',
                        config:{},
                        callback: function () { }
                    })
                }, function (result) {
                    $log.info('Modal dismissed at: ' + new Date());
                    $log.info(result);
                });
            })
        };
        $scope.add = function (size) {
            $scope.title = "添加用户";
            $scope.selectedUser = {};
            var modalInstance = $modal.open({
                templateUrl: 'scripts/func/user/template/add.html',
                controller: function ($scope, $modalInstance, title, user) {

                    $scope.title = title;
                    $scope.user = user;

                    $scope.ok = function () {
                        $scope.$emit(Config.events.loading, {
                            type: 'loading'
                        })
                        console.log($scope.user)
                        var result = UserService.save($scope.user, function () {
                            $modalInstance.close(result);
                            $scope.$emit(Config.events.loading, {
                                type: 'loaded'
                            });
                        })

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.reset = function () {
                        $scope.user = {};
                    }
                },
                size: size,
                resolve: {
                    title: function () {
                        return $scope.title;
                    },
                    user: function () {
                        return $scope.selectedUser;
                    }
                }
            });

            modalInstance.result.then(function (result) {
                $log.info(result);
                $scope.search();
                $scope.$emit(Config.events.showMsg, {
                    type: 'success',
                    title: '操作成功',
                    content: '获取数据成功',
                    config:{},
                    callback: function () { }
                })
            }, function (result) {
                $log.info('Modal dismissed at: ' + new Date());
                $log.info(result);
            });

        };

    }])
    ;
});