/**
 * Created by three on 14-8-6.
 */
define(['angular', 'func/base/config'], function (angular) {
    'use strict';

    /**
     * @ngdoc function
     * @name appApp.service:UserLoginService
     * @description
     * # UserLoginService
     * Controller of the appApp
     */
    angular.module('app.func.UserLoginService', [
        'app.func.config'
    ])
        .factory('UserLoginService', ['$resource','$window','Config', function ($resource,$window, config) { // restful
            return $resource(config.data_serve_base_url+'/user/login',
                {},
                {
                    post: {
                        method: 'POST', params: {},
                        isArray: false,
                        interceptor: {
                            response: function (data) {
                                console.log('response in interceptor', data);
                            },
                            responseError: function (data) {
                                console.log('error in interceptor', data);
                            }
                        }
                    }
                }
            );
        }])
        .factory('UserLogoutService', ['$resource', function ($resource) { // restful
            return $resource('http://127.0.0.1:1234/system/messages',
                {},
                {
                    query: {method: 'POST', params: {}, isArray: true}
                }
            );
        }])
    ;
});