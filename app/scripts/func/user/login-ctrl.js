define(['angular', 'func/user/login-serv', 'func/base/service', 'func/base/config'], function (angular) {
    'use strict';

    /**
     * @ngdoc function
     * @name appApp.controller:FuncUserLoginCtrl
     * @description
     * # FuncUserLoginCtrl
     * Controller of the appApp
     */
    angular.module('app.func.UserLoginCtrl', [
        'app.func.UserLoginService',
        'app.func.common.service'
    ])
        .controller('UserPageLoginCtrl', ['$scope', '$location', '$http','$window','Config','UserLoginService','AuthenticationService',  function ($scope, $location, $http,$window,config, UserLoginService, AuthenticationService) {

            $scope.login_result = {
                flag: true,
                code:'',
                msg:''
            };
            $scope.login = function(){

                $window.sessionStorage.token = AuthenticationService.make_basic_auth($scope.user.username, $scope.user.password);
                $scope.$emit(config.events.loading,{type:'loading'})
                var data = UserLoginService.post($scope.user, function () {
                    if(data.code=='0000') {
                        AuthenticationService.isLogin = true;
                        AuthenticationService.loginUser(data.result);
                        location.href = 'index.html';
                    } else {
                        $scope.setImage();
                        AuthenticationService.isLogin = false;
                        $window.sessionStorage.token = null;
                        $scope.login_result = data;
                        $scope.login_result.flag = false;
                    }
                    $scope.$emit(config.events.loading,{type:'loaded'})
                });

                return false;
            };

            $scope.closeErrorDlg = function() {
                $scope.login_result.flag = true;
            }
            var vaildCodeUrl=config.data_serve_base_url+"/vaildCode.jpg"
            $scope.vaildCode=vaildCodeUrl+"?t="+new Date().getTime()
            $scope.setImage=function(){
                $scope.vaildCode =vaildCodeUrl+"?t="+new Date().getTime();
            }
        }])
        .controller('UserAjaxLoginCtrl', ['$scope', '$location', '$http','$window','UserLoginService','AuthenticationService',  function ($scope, $location, $http,$window, UserLoginService, AuthenticationService) {
            $scope.login_result = {
                flag: true,
                code:'',
                msg:''
            };
            $scope.login = function(){

                $window.sessionStorage.token = AuthenticationService.make_basic_auth($scope.user.username, $scope.user.password,$scope.user.captcha);

                var data = UserLoginService.post($scope.user, function () {
                    if(data.code=='0000') {
                        AuthenticationService.isLogin = true;
                    } else {
                        AuthenticationService.isLogin = false;
                        $window.sessionStorage.token = null;
                        $scope.login_result = data;
                        $scope.login_result.flag = false;
                    }
                });

                return false;
            };
            $scope.closeErrorDlg = function() {
                $scope.login_result.flag = true;
            }
        }])
    ;
});
