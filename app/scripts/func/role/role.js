/**
 * Created by three on 14-9-11.
 */
define(['angular', 'func/base/config','func/base/service', 'ng/ng.plugins'], function (angular) {
    'use strict';

    angular.module('app.func.role', [
        'app.func.config',
        'app.plugins',
        'app.func.common.service'
    ]).controller('RoleCtrl', [
        '$log','$scope', '$compile' ,'$debounce','$window','$modal','Config','ConfigConstantService',
        function ($log, $scope, $compile,$debounce,$window,$modal,config, ConfigConstant) {
            $scope.$emit(config.events.loading, { type: 'loading' });

            // 页面操作功能
            var pageBut = [
                {
                    url: 'config.collapseQuery=!config.collapseQuery;',
                    title: '查询', icon: 'fa fa-search',
                    action: function () {
                        $scope.config.collapseQuery=!$scope.config.collapseQuery;
                    }
                }
            ];
            $scope.listTop.push(pageBut);
            $scope.config.collapseQuery=true;
            $scope.config.title = '角色管理';
            for(var i=0; i<pageBut.length; i++) {
                $scope.$on(pageBut[i].url, pageBut[i].action)
            }

            /**
             * 配置datatable参数
             * @returns {*}
             */
            $scope.getTableConfig = function () {

                return $scope.getAuthTableConfig(config.data_serve_base_url+'/user', {
                    aoColumns: [
                        { "sTitle": "用户名", "mDataProp": "name" },
                        { "sTitle": "用户密码", "mDataProp": "pwd" },
                        { "sTitle": "管理",
                            "sClass": "center",
                            "bSortable": false,
                            "mDataProp": null,
                            "mData":null,
                            "render": $scope.manageColumnT2
                        }
                    ],
                    fnServerParams: function ( aoData ) {
                        if($scope.user) {
                            for(var k in $scope.user) {
                                aoData[k] = $scope.user[k]
                            }
                        }
                    }
                });

            };

            $scope.notifyResult = function (result, cb) {
                if('SUCCESS'==result.code) {
                    $scope.$emit(config.events.showMsg, {
                        type: 'success',
                        title: '操作成功',
                        content: result.code+' : '+result.msg,
                        config:{},
                        callback: cb
                    })
                } else {
                    $scope.$emit(config.events.showMsg, {
                        type: 'error',
                        title: '操作失败',
                        content: result.code+' : '+result.msg,
                        config:{},
                        callback: cb
                    })
                }
            };

            $scope.$emit(config.events.loading, { type: 'loaded' });
        }])
    ;
});