/*jshint unused: vars */
require.config({
//    basePath: '../func',
    paths: {
        'JSON':'../bower_components/json3/lib/json3.min',
        'jquery': '../bower_components/jquery/dist/jquery.min',
        'jquery-easing':'plugin/jquery-easing/jquery.easing.1.3',
        'jquery-ui':'../bower_components/jquery-ui/jquery-ui.min',
        'bootstrap': '../bower_components/bootstrap/dist/js/bootstrap.min',
        'angular': '../bower_components/angular/angular.min',
        'angular-bootstrap': '../bower_components/angular-bootstrap/ui-bootstrap-tpls.min',
        'angular-route': '../bower_components/angular-route/angular-route.min',
        'angular-cookies': '../bower_components/angular-cookies/angular-cookies.min',
        'angular-sanitize': '../bower_components/angular-sanitize/angular-sanitize.min',
        'angular-resource': '../bower_components/angular-resource/angular-resource.min',
        'angular-animate': '../bower_components/angular-animate/angular-animate.min',
        'angular-touch': '../bower_components/angular-touch/angular-touch.min',
        'angular-mocks': '../bower_components/angular-mocks/angular-mocks',
        'socket.io':'../bower_components/socket.io-client/socket.io',
        'sockjs-client':'../bower_components/sockjs-client/dist/sockjs',
        'stomp-websocket':'../bower_components/stomp-websocket/lib/stomp.min',
        'ztree': 'plugin/zTree_v3/js/jquery.ztree.all-3.5.min',
        'datatables/bootstrap': 'plugin/datatables/dataTables.bootstrap.min',
        'datatables/jquery': 'plugin/datatables/jquery.dataTables.min',
        'datatables/colVis': 'plugin/datatables/dataTables.colVis.min',
        'datatables/tools': 'plugin/datatables/dataTables.tableTools.min',
        'datatables': 'plugin/datatable-responsive/datatables.responsive.min',
        'app': 'app',
        'app.init':'app.init'
    },
    shim: {
        'datatables': {
            deps: [
                'datatables/bootstrap'
            ],
            exports: 'datatables'
        },
        'datatables/bootstrap': ['datatables/tools' ],
        'datatables/tools': [ 'datatables/colVis' ],
        'datatables/colVis': [ 'datatables/jquery' ],
        'datatables/jquery': [ 'jquery' ],
        'angular': {
            deps: ['jquery'],
            'exports': 'angular'
        },
        'angular-bootstrap': ['angular'],
        'angular-route': ['angular'],
        'angular-cookies': ['angular'],
        'angular-sanitize': ['angular'],
        'angular-resource': ['angular'],
        'angular-animate': ['angular'],
        'angular-touch': ['angular'],
        'angular-mocks': {
            deps: ['angular'],
            'exports': 'angular.mock'
        },
        'stomp-websocket':{
            deps: ['sockjs-client'],
            exports: 'stomp-websocket'
        },
        'app': {
            deps: ['app.config', 'app.init', 'jquery', 'angular', 'utils/uuid', 'utils/toolbox'],
            'exports': 'app'
        },
        'app.init':['jquery','bootstrap','app.config'],
        'bootstrap':['jquery'],
        'jquery-easing':['jquery']
    },
    priority: [
        'angular'
    ]
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = 'NG_DEFER_BOOTSTRAP!';

require([
    'angular',
    'angular-bootstrap',
    'JSON',
//    'speech/voicecommand.min',
    'app',
    'angular-route',
    'angular-cookies',
    'angular-sanitize',
    'angular-resource',
    'angular-animate',
    'angular-touch'
], function (angular, app, ngRoutes, ngCookies, ngSanitize, ngResource, ngAnimate, ngTouch) {
    'use strict';

    // init page

    /* jshint ignore:start */
    var $html = angular.element(document.getElementsByTagName('html')[0]);
    /* jshint ignore:end */
    angular.element().ready(function () {
        angular.resumeBootstrap(['app']);
    });
});
