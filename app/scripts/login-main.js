/*jshint unused: vars */
require.config({
  paths: {
      'jquery': '../bower_components/jquery/dist/jquery.min',
      'angular' : '../bower_components/angular/angular.min',
      'angular-route': '../bower_components/angular-route/angular-route.min',
      'angular-cookies': '../bower_components/angular-cookies/angular-cookies.min',
      'angular-sanitize': '../bower_components/angular-sanitize/angular-sanitize.min',
      'angular-resource': '../bower_components/angular-resource/angular-resource.min',
      'angular-animate': '../bower_components/angular-animate/angular-animate.min',
      'angular-touch': '../bower_components/angular-touch/angular-touch.min',
      'angular-mocks': '../bower_components/angular-mocks/angular-mocks'
  },
  shim: {
    'angular' : {
        'deps':['jquery'],
        'exports' : 'angular'
    },
    'angular-route': ['angular'],
    'angular-cookies': ['angular'],
    'angular-sanitize': ['angular'],
    'angular-resource': ['angular'],
    'angular-mocks': {
      deps:['angular'],
      'exports':'angular.mock'
    }
  },
  priority: [
    'angular'
  ]
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = 'NG_DEFER_BOOTSTRAP!';

require([
  'angular',
  'login-app',
  'angular-route',
  'angular-cookies',
  'angular-sanitize',
  'angular-resource'
], function(angular, ngRoutes, ngCookies, ngSanitize, ngResource) {
  'use strict';

  /* jshint ignore:start */
  var $html = angular.element(document.getElementsByTagName('html')[0]);
//  /* jshint ignore:end */
  angular.element().ready(function() {
    angular.resumeBootstrap(['appLogin']);
  });

//    angular.element("html").ready(function() {
//        angular.bootstrap("html",['adminApp']);
//    });
});
