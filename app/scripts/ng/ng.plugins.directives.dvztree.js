define(['angular', 'ng/ng.plugins','ztree'], function (angular) {
    'use strict';

    /**
     * Created by three on 14-7-22.
     */

    angular.module('app.plugins')
        .directive('dvZtree', [function () {
            return {
                restrict: 'EA',
                scope: {
                    watch:'@',
                    nodesData:'=nodesData',
                    dvClick:'&',
                    obj:'=dvObj'
                },
                link: function (scope, element, attrs, controller) {
                    if(!attrs.id){//判断ID是否存在，自动生成
                        attrs.id = Math.uuid();
                        element.attr('id',attrs.id);
                    }

                    //console.log('ztree封装需要实现')
                    var setting = scope.nodesData.setting;
                    var zNodes = scope.nodesData.zNodes;
                    if(setting.callback==null) setting.callback={}
                    setting.callback.onClick = function (event, treeId, treeNode) {
                        if(typeof scope.dvClick=='function') {
                            scope.dvClick({
                                id:treeNode.id,
                                pId:treeNode.pId,
                                name: treeNode.name
                            })
                        }
                    };

                    $.fn.zTree.init(element,setting,zNodes);
                    scope.obj = $.fn.zTree.getZTreeObj(attrs.id);

                    scope.$watch('watch', function () {
                        scope.obj.destroy();
                        $.fn.zTree.init(element,setting,zNodes);
                        scope.obj = $.fn.zTree.getZTreeObj(attrs.id);
                    });
                }
            };
        }])
    ;
});
