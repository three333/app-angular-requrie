define(['angular', 'ng/ng.plugins'], function (angular) {
    'use strict';

    /**
     * Created by three on 14-7-22.
     */

    angular.module('app.plugins')
        .directive('dvTree', [function () {
            return {
                restrict: 'EA',
                replace: true,
                scope: {
                    config: '&',
                    treeNodes: '@treeNodes',
                    nodeClick: '&',
                    nodeRender: '&'
                },
                link: function (scope, element, attrs) {

                    // 指令初始化
                    function init() {
                        var treeData = scope.config();
                        element.html('');
                        element.append(createTree(treeData, true));
                        bindEvent();
                    }

                    // 监听数据改变
                    scope.$watch('treeNodes', function () {
                        init();
                    });

                    // 创建树
                    function createTree(nodes) {
                        var $_tree = $('<ul></ul>');
                        for (var i = 0; i < nodes.length; i++) {
                            $_tree.append(createNode(nodes[i], true));
                        }
                        return $_tree;
                    }

                    // 生成元素
                    function createNode(node, is_show) {
                        var $_node = $('\
                            <li>\
                                <span>\
                                <i></i>\
                                </span>\
                                <ul></ul>\
                            </li>');
                        var $_node_info = $_node.find('> span');
                        var $_node_icon = $_node_info.find('> i');
                        var $_childNodes = $_node.find('> ul');

                        var none = is_show ? $_node.show() : $_node.hide();

                        $_node_info.attr({
                            'refresh-icon': node['refresh-icon'],
                            'extend-icon': node['extend-icon'],
                            'collapse-icon': node['collapse-icon']
                        });

                        $_node_info.append(scope.nodeRender({node: node}) || node.title);
                        $_node_icon.removeClass().addClass(node.icon);
                        $_node_info.on('click', function () {
                            scope.nodeClick({
                                element: $_node_info,
                                node: node
                            });
                        });

                        for (var i = 0; node.child && i < node.child.length; i++) {
                            $_childNodes.append(createNode(node.child[i], node.is_open));
                        }

                        return $_node;
                    }

                    // 绑定事件
                    function bindEvent() {
                        element.find('> ul').attr('role', 'tree').find('ul').attr('role', 'group');
                        element.find('ul').each(function () {
                            var $_this = $(this);
                            if ($_this.children('li').length == 0) {
                                $_this.remove();
                            }
                        });

                        element.find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', '收缩').on('click', function (e) {
                            var $_this = $(this);
                            var $_this_iclass = $_this.find('> i').attr("class");

                            var refresh = $_this.attr("refresh-icon") || $_this_iclass;
                            var extend = $_this.attr("extend-icon") || $_this_iclass;
                            var collapse = $_this.attr("collapse-icon") || $_this_iclass;

                            var children = $(this).parent('li.parent_li').find(' > ul > li');
                            if (children.length > 0) {
                                $_this.attr('title', '展开').find('> i').removeClass().addClass(refresh);
                                if (children.is(':visible')) {
                                    children.hide('fast', function () {
                                        $_this.attr('title', '展开').find('> i').removeClass().addClass(collapse);
                                    });
                                } else {
                                    children.show('fast', function () {
                                        $_this.attr('title', '收缩').find('> i').removeClass().addClass(extend);
                                    });
                                }
                            }
                            e.stopPropagation();
                        });
                    }


                }
            };
        }])

    ;
});