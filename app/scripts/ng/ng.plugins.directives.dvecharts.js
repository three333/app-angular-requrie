define(['angular', 'ng/ng.plugins'], function (angular) {
    'use strict';

    /**
     * Created by three on 14-7-22.
     */

    angular.module('app.plugins')
        .directive('dvEcharts', [function () {

            return {
                restrict: 'A',
                scope: {
                    config: '&',
                    chart: '='
                },
                compile: function (element, attrs, transcludeFn) {
                    if (!attrs.chart) { // 自动生成id
                        attrs.chart = "chart" + Math.uuid().replace(/-/g, '_');
                        element.attr('chart', attrs.chart);
                    }
                    // 返回链接函数，link参数不会调用
                    return {
                        pre: function (element, attrs, controller) {

                        },
                        post: function (scope, element, attrs, controller) {
                            if (!attrs.id) { // 自动生成id
                                attrs.id = Math.uuid();
                                element.attr('id', attrs.id);
                            }

                            scope.chart = window.echarts.init(document.getElementById(attrs.id));

                            var option = scope.config() || {};

                            // 为echarts对象加载数据
                            scope.chart.setOption(option);
                        }
                    };
                }
            };

        }])

    ;
});