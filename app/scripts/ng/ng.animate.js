/**
 * Created by three on 14-8-12.
 */
define(['angular'/*, 'laydate', 'layer'*/], function (angular) {
    'use strict';

    /**
     * angular animate defined
     */
    angular.module('app.animate', ['ngAnimate']);
});