define(['angular', 'ng/ng.plugins', 'datatables'], function (angular) {
    'use strict';



    /**
     * Created by three on 14-7-22.
     */
    angular.module('app.plugins')
        .directive('dvDataTable', ['$debounce','$compile', function ($debounce, $compile) {
            return {
                restrict: 'A',
                scope: {
                    config: '&',
                    table: '='
                },
                link: function (scope, element, attrs) {

                    if (!attrs.id) { // 自动生成id
                        attrs.id = Math.uuid();
                        element.attr('id', attrs.id);
                    }

                    // pagefunction
                    var pagefunction = function () {
                        var config = {
                            "oLanguage": {
                                "sProcessing": "处理中...",
                                "sLengthMenu": "每页显示  _MENU_ 条记录",
                                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                                "oPaginate": {
                                    "sFirst": "首页",
                                    "sPrevious": "前一页",
                                    "sNext": "后一页",
                                    "sLast": "尾页"
                                },
                                "sZeroRecords": "抱歉， 没有找到",
                                "sInfoEmpty": "没有数据"
                            }
                        };

                        if (scope.config()) {
                            for (var k in scope.config()) {
                                config[k] = scope.config()[k];
                            }
                        }
                        config.initComplete = function () {
                            $compile(element.children())(scope.$parent);
                        };
                        scope.table = element.dataTable(config);
                    };
                    $debounce(pagefunction, 500);

                }
            };
        }])
    ;

});