define(['angular','notification/MsgBoxNotification'], function (angular) {
    'use strict';

// plugins DIRECTIVES
// main directives

    var app_plugins_directives = angular.module('app.plugins', [])
        .factory('IconSelectService', ['$compile', function ($compile) {

            var iconGroups = { 'Unkown Icons': [ 'fa-automobile', 'fa-bank', 'fa-behance', 'fa-behance-square', 'fa-bomb', 'fa-building', 'fa-cab', 'fa-car', 'fa-child', 'fa-circle-o-notch', 'fa-circle-thin', 'fa-codepen', 'fa-cube', 'fa-cubes', 'fa-database', 'fa-delicious', 'fa-deviantart', 'fa-digg', 'fa-drupal', 'fa-empire', 'fa-envelope-square', 'fa-fax', 'fa-file-archive-o', 'fa-file-audio-o', 'fa-file-code-o', 'fa-file-excel-o', 'fa-file-image-o', 'fa-file-movie-o', 'fa-file-pdf-o', 'fa-file-photo-o', 'fa-file-picture-o', 'fa-file-powerpoint-o', 'fa-file-sound-o', 'fa-file-video-o', 'fa-file-word-o', 'fa-file-zip-o', 'fa-ge', 'fa-git', 'fa-git-square', 'fa-google', 'fa-graduation-cap', 'fa-hacker-news', 'fa-header', 'fa-history', 'fa-institution', 'fa-joomla', 'fa-jsfiddle', 'fa-language', 'fa-life-bouy', 'fa-life-ring', 'fa-life-saver', 'fa-mortar-board', 'fa-openid', 'fa-paper-plane', 'fa-paper-plane-o', 'fa-paragraph', 'fa-paw', 'fa-pied-piper', 'fa-pied-piper-alt', 'fa-pied-piper-square', 'fa-qq', 'fa-ra', 'fa-rebel', 'fa-recycle', 'fa-reddit', 'fa-reddit-square', 'fa-send', 'fa-send-o', 'fa-share-alt', 'fa-share-alt-square', 'fa-slack', 'fa-sliders', 'fa-soundcloud', 'fa-space-shuttle', 'fa-spoon', 'fa-spotify', 'fa-steam', 'fa-steam-square', 'fa-stumbleupon', 'fa-stumbleupon-circle', 'fa-support', 'fa-taxi', 'fa-tencent-weibo', 'fa-tree', 'fa-university', 'fa-vine', 'fa-wechat', 'fa-weixin', 'fa-wordpress', 'fa-yahoo'    ], 'Web Application Icons': [ 'fa-adjust', 'fa-anchor', 'fa-archive', 'fa-asterisk', 'fa-ban', 'fa-bar-chart-o', 'fa-barcode', 'fa-beer', 'fa-bell', 'fa-bell-o', 'fa-bolt', 'fa-book', 'fa-bookmark', 'fa-bookmark-o', 'fa-briefcase', 'fa-bug', 'fa-building', 'fa-bullhorn', 'fa-bullseye', 'fa-calendar', 'fa-calendar-o', 'fa-camera', 'fa-camera-retro', 'fa-caret-square-o-down', 'fa-caret-square-o-left', 'fa-caret-square-o-right', 'fa-caret-square-o-up', 'fa-certificate', 'fa-check', 'fa-check-circle', 'fa-check-circle-o', 'fa-check-square', 'fa-check-square-o', 'fa-circle', 'fa-circle-o', 'fa-clock-o', 'fa-cloud', 'fa-cloud-download', 'fa-cloud-upload', 'fa-code', 'fa-code-fork', 'fa-coffee', 'fa-cog', 'fa-cogs', 'fa-plus-square-o', 'fa-comment', 'fa-comment-o', 'fa-comments', 'fa-comments-o', 'fa-compass', 'fa-credit-card', 'fa-crop', 'fa-crosshairs', 'fa-cutlery', 'fa-dashboard', 'fa-desktop', 'fa-dot-circle-o', 'fa-download', 'fa-edit', 'fa-ellipsis-horizontal', 'fa-ellipsis-vertical', 'fa-envelope', 'fa-envelope-o', 'fa-eraser', 'fa-exchange', 'fa-exclamation', 'fa-exclamation-circle', 'fa-exclamation-triangle', 'fa-minus-square-o', 'fa-external-link', 'fa-external-link-square', 'fa-eye', 'fa-eye-slash', 'fa-female', 'fa-fighter-jet', 'fa-film', 'fa-filter', 'fa-fire', 'fa-fire-extinguisher', 'fa-flag', 'fa-flag-checkered', 'fa-flag-o', 'fa-flash', 'fa-flask', 'fa-folder', 'fa-folder-o', 'fa-folder-open', 'fa-folder-open-o', 'fa-frown-o', 'fa-gamepad', 'fa-gavel', 'fa-gear', 'fa-gears', 'fa-gift', 'fa-glass', 'fa-globe', 'fa-group', 'fa-hdd-o', 'fa-headphones', 'fa-heart', 'fa-heart-o', 'fa-home', 'fa-inbox', 'fa-info', 'fa-info-circle', 'fa-key', 'fa-keyboard-o', 'fa-laptop', 'fa-leaf', 'fa-legal', 'fa-lemon-o', 'fa-level-down', 'fa-level-up', 'fa-lightbulb-o', 'fa-location-arrow', 'fa-lock', 'fa-magic', 'fa-magnet', 'fa-mail-forward', 'fa-mail-reply', 'fa-mail-reply-all', 'fa-male', 'fa-map-marker', 'fa-meh-o', 'fa-microphone', 'fa-microphone-slash', 'fa-minus', 'fa-minus-circle', 'fa-minus-square', 'fa-minus-square-o', 'fa-mobile', 'fa-mobile-phone', 'fa-money', 'fa-moon-o', 'fa-move', 'fa-music', 'fa-pencil', 'fa-pencil-square', 'fa-pencil-square-o', 'fa-phone', 'fa-phone-square', 'fa-picture-o', 'fa-plane', 'fa-plus', 'fa-plus-circle', 'fa-plus-square', 'fa-power-off', 'fa-print', 'fa-puzzle-piece', 'fa-qrcode', 'fa-question', 'fa-question-circle', 'fa-quote-left', 'fa-quote-right', 'fa-random', 'fa-refresh', 'fa-reorder', 'fa-reply', 'fa-reply-all', 'fa-resize-horizontal', 'fa-resize-vertical', 'fa-retweet', 'fa-road', 'fa-rocket', 'fa-rss', 'fa-rss-square', 'fa-search', 'fa-search-minus', 'fa-search-plus', 'fa-share', 'fa-share-square', 'fa-share-square-o', 'fa-shield', 'fa-shopping-cart', 'fa-sign-in', 'fa-sign-out', 'fa-signal', 'fa-sitemap', 'fa-smile-o', 'fa-sort', 'fa-sort-alpha-asc', 'fa-sort-alpha-desc', 'fa-sort-amount-asc', 'fa-sort-amount-desc', 'fa-sort-asc', 'fa-sort-desc', 'fa-sort-down', 'fa-sort-numeric-asc', 'fa-sort-numeric-desc', 'fa-sort-up', 'fa-spinner', 'fa-square', 'fa-square-o', 'fa-star', 'fa-star-half', 'fa-star-half-empty', 'fa-star-half-full', 'fa-star-half-o', 'fa-star-o', 'fa-subscript', 'fa-suitcase', 'fa-sun-o', 'fa-superscript', 'fa-tablet', 'fa-tachometer', 'fa-tag', 'fa-tags', 'fa-tasks', 'fa-terminal', 'fa-thumb-tack', 'fa-thumbs-down', 'fa-thumbs-o-down', 'fa-thumbs-o-up', 'fa-thumbs-up', 'fa-ticket', 'fa-times', 'fa-times-circle', 'fa-times-circle-o', 'fa-tint', 'fa-toggle-down', 'fa-toggle-left', 'fa-toggle-right', 'fa-toggle-up', 'fa-trash-o', 'fa-trophy', 'fa-truck', 'fa-umbrella', 'fa-unlock', 'fa-unlock-alt', 'fa-unsorted', 'fa-upload', 'fa-user', 'fa-video-camera', 'fa-volume-down', 'fa-volume-off', 'fa-volume-up', 'fa-warning', 'fa-wheelchair', 'fa-wrench'    ], 'Form Control Icons': [ 'fa-check-square', 'fa-check-square-o', 'fa-circle', 'fa-circle-o', 'fa-dot-circle-o', 'fa-minus-square', 'fa-minus-square-o', 'fa-square', 'fa-square-o'    ], 'Currency Icons': [ 'fa-bitcoin', 'fa-btc', 'fa-cny', 'fa-dollar', 'fa-eur', 'fa-euro', 'fa-gbp', 'fa-inr', 'fa-jpy', 'fa-krw', 'fa-money', 'fa-rmb', 'fa-rouble', 'fa-rub', 'fa-ruble', 'fa-rupee', 'fa-try', 'fa-turkish-lira', 'fa-usd', 'fa-won', 'fa-yen'    ], 'Text Editor Icons': [ 'fa-align-center', 'fa-align-justify', 'fa-align-left', 'fa-align-right', 'fa-bold', 'fa-chain', 'fa-chain-broken', 'fa-clipboard', 'fa-columns', 'fa-copy', 'fa-cut', 'fa-dedent', 'fa-eraser', 'fa-file', 'fa-file-o', 'fa-file-text', 'fa-file-text-o', 'fa-files-o', 'fa-floppy-o', 'fa-font', 'fa-indent', 'fa-italic', 'fa-link', 'fa-list', 'fa-list-alt', 'fa-list-ol', 'fa-list-ul', 'fa-outdent', 'fa-paperclip', 'fa-paste', 'fa-repeat', 'fa-rotate-left', 'fa-rotate-right', 'fa-save', 'fa-scissors', 'fa-strikethrough', 'fa-table', 'fa-text-height', 'fa-text-width', 'fa-th', 'fa-th-large', 'fa-th-list', 'fa-underline', 'fa-undo', 'fa-unlink'    ], 'Directional Icons': [ 'fa-angle-double-down', 'fa-angle-double-left', 'fa-angle-double-right', 'fa-angle-double-up', 'fa-angle-down', 'fa-angle-left', 'fa-angle-right', 'fa-angle-up', 'fa-arrow-circle-down', 'fa-arrow-circle-left', 'fa-arrow-circle-o-down', 'fa-arrow-circle-o-left', 'fa-arrow-circle-o-right', 'fa-arrow-circle-o-up', 'fa-arrow-circle-right', 'fa-arrow-circle-up', 'fa-arrow-down', 'fa-arrow-left', 'fa-arrow-right', 'fa-arrow-up', 'fa-caret-down', 'fa-caret-left', 'fa-caret-right', 'fa-caret-square-o-down', 'fa-caret-square-o-left', 'fa-caret-square-o-right', 'fa-caret-square-o-up', 'fa-caret-up', 'fa-chevron-circle-down', 'fa-chevron-circle-left', 'fa-chevron-circle-right', 'fa-chevron-circle-up', 'fa-chevron-down', 'fa-chevron-left', 'fa-chevron-right', 'fa-chevron-up', 'fa-hand-o-down', 'fa-hand-o-left', 'fa-hand-o-right', 'fa-hand-o-up', 'fa-long-arrow-down', 'fa-long-arrow-left', 'fa-long-arrow-right', 'fa-long-arrow-up', 'fa-toggle-down', 'fa-toggle-left', 'fa-toggle-right', 'fa-toggle-up'    ], 'Video Player Icons': [ 'fa-backward', 'fa-eject', 'fa-fast-backward', 'fa-fast-forward', 'fa-forward', 'fa-arrows-alt', 'fa-pause', 'fa-play', 'fa-play-circle', 'fa-play-circle-o', 'fa-expand', 'fa-compress', 'fa-step-backward', 'fa-step-forward', 'fa-stop', 'fa-youtube-play'    ], 'Brand Icons': [ 'fa-adn', 'fa-android', 'fa-apple', 'fa-bitbucket', 'fa-bitbucket-square', 'fa-bitcoin', 'fa-btc', 'fa-css3', 'fa-dribbble', 'fa-dropbox', 'fa-facebook', 'fa-facebook-square', 'fa-flickr', 'fa-foursquare', 'fa-github', 'fa-github-alt', 'fa-github-square', 'fa-gittip', 'fa-google-plus', 'fa-google-plus-square', 'fa-html5', 'fa-instagram', 'fa-linkedin', 'fa-linkedin-square', 'fa-linux', 'fa-maxcdn', 'fa-pagelines', 'fa-pinterest', 'fa-pinterest-square', 'fa-renren', 'fa-skype', 'fa-stack-exchange', 'fa-stack-overflow', 'fa-trello', 'fa-tumblr', 'fa-tumblr-square', 'fa-twitter', 'fa-twitter-square', 'fa-vimeo-square', 'fa-vk', 'fa-weibo', 'fa-windows', 'fa-xing', 'fa-xing-square', 'fa-youtube', 'fa-youtube-play', 'fa-youtube-square'    ], 'Medical Icons': [ 'fa-ambulance', 'fa-h-square', 'fa-hospital-o', 'fa-medkit', 'fa-plus-square', 'fa-stethoscope', 'fa-user-md', 'fa-wheelchair']};
            var tabset = '<tabset justified="true">';
            for(var group in iconGroups) {
                var row = '<div class="row">'
                for(var i=0; i<iconGroups[group].length;i++) {
                    var icon = iconGroups[group][i];
                    row += '<div class="col-xs-6 col-md-3 col-sm-4 select-icon-font" style="cursor: pointer" ng-click="selectIcon(\''+icon+'\')"> \
                            <i class="fa '+icon+'"></i> '+icon+' \
                            </div>';
                }
                row += '</div>';
                tabset += '<tab heading="'+group+'('+iconGroups[group].length+')">'+row+'</tab>';
            }
            tabset += '</tabset>';

                /*
                 var icon_body = angular.element('<div></div>');
                 for(var group in iconGroups) {
                 icon_body.append('<h2>'+group+'('+iconGroups[group].length+')</h2>');
                 var row = '<div class="row">'
                 for(var i=0; i<iconGroups[group].length;i++) {
                 var icon = iconGroups[group][i];
                 row += '<div class="col-xs-6 col-md-3 col-sm-4 select-icon-font" style="cursor: pointer" ng-click="selectIcon(\''+icon+'\')"> \
                 <i class="fa '+icon+'"></i> '+icon+' \
                 </div>';
                 }
                 row += '</div>';
                 icon_body.append(row);
                 }
                 */

            var html = '<div class="icon-select-modal" > \
                            <div class="icon-select-title">图标选择 \
                                <spa>[选择的图标: <i class="{{selectedIcon}}"></i> {{selectedIcon}}]</spa>\
                                <span ng-click="ok()" class="icon-select-button" title="确认">确认</span>\
                                <span ng-click="close()" class="icon-select-button" title="确认">取消</span>\
                                <span ng-click="rest()" class="icon-select-button" title="确认">重置</span>\
                                <span ng-click="clear()" class="icon-select-button" title="确认">清空</span>\
                                <span class="icon-select-button">\
                                    <label><input type="checkbox"  ng-model="falg" ng-true-value="fa-lg " ng-false-value="" ng-change="setopt()"> fa-lg</label>\
                                </span>\
                                <span class="icon-select-button">\
                                    <label><input type="checkbox" ng-model="fafw" ng-true-value="fa-fw " ng-false-value="" ng-change="setopt()"> fa-fw</label>\
                                </span>\
                            </div> \
                            <div class="icon-select-body"> \
                                '+tabset+' \
                            </div> \
                        </div>';

            return function (scope) {
                var elm;
                return {
                    open: function() {
                        elm = angular.element(html);
                        angular.element(document.body).prepend(elm);
                        $compile(elm)(scope);
                    },
                    close: function() {
                        if (elm) {
                            elm.remove();
                        }
                    }
                }
            }

        }])
        .directive('dvIconSelect', ['IconSelectService', function (IconSelectService) {

            return {
                restrict: 'A',
                require: '?ngModel',
                scope:{},
                link: function (scope, element, attr, ngModel) {
                    if(!ngModel)
                        return;
                    element.css({cursor: 'pointer'});

                    var modal = new IconSelectService(scope);

                    var prefix = 'fa ';
                    scope.falg ='';
                    scope.fafw ='';
                    scope.icon = '';

                    scope.setopt = function () {
                        scope.selectedIcon = prefix +scope.falg +' '+scope.fafw +' '+scope.icon;
                    };

                    scope.selectIcon = function(icon) {
                        scope.icon = icon;
                        scope.selectedIcon = prefix +scope.falg +' '+scope.fafw +' '+scope.icon;

                    };
                    scope.ok = function () {
                        ngModel.$setViewValue(scope.selectedIcon);
                        scope.close();
                    };

                    scope.close = function () {
                        modal.close();
                    };

                    scope.rest = function () {
                        scope.selectedIcon = scope.oldIcon;
                        var a = scope.selectedIcon.split(' ')
                        scope.icon = a[a.length-1];
                    };

                    scope.clear = function () {
                        scope.icon = '';
                        scope.selectedIcon = '';
                    };

                    element.on('click', function () {
                        scope.oldIcon = scope.selectedIcon = ngModel.$modelValue;
                        var a = scope.selectedIcon.split(' ')
                        scope.icon = a[a.length-1];
                        modal.open();
                    });
                }
            };
        }])
        // 加载层
        .directive('dvLayer', ['$rootScope','LoadingService', function ($rootScope, LoadingService) {
            return {
                link: function (scope, element, attr, controller) {
                    element.hide('hide');

                    $rootScope.$on('$routeChangeStart', function () {
                        LoadingService.loading();
                    });

                    $rootScope.$on('$routeChangeSuccess', function () {
                        LoadingService.loaded();
                    });
                }
            };
        }])
        // 消息提示
        .factory('MessageBox', ['$rootScope', function ($rootScope) {
            return {
                modelessBox: {
                    box: function (config, callback) {
                        jQuery.smallBox(config, callback);
                    },
                    bigbox: function (config, callback) {
                        jQuery.bigBox(config, callback);
                    },
                    success: function (title, content, options, callback) {
                        var conf = {
                            title : title || '成功',
                            content : "<i>"+content+"</i>",
                            color : "#296191",
                            iconSmall : "fa fa-thumbs-up bounce animated",
                            timeout : 4000
                        }
                        conf = jQuery.extend(conf,options)
                        this.box(conf, callback);
                    },
                    info: function (title, content, options, callback) {
                        var conf = {
                            title : title || '提示',
                            content : "<i>"+content+"</i>",
                            color : "#00cc00",
                            iconSmall : "fa fa-info bounce animated",
                            timeout : 4000
                        }
                        conf = jQuery.extend(conf,options)
                        this.box(conf, callback);
                    },
                    warn: function (title, content, options, callback) {
                        var conf = {
                            title : title || '警告',
                            content : "<i>"+content+"</i>",
                            color : "#888888",
                            iconSmall : "fa fa-warning bounce animated",
                            timeout : 4000
                        }
                        // 闪烁处理,使用背景切换
                        conf.colors = ["#888888","#555555"]
                        conf.colortime = 200;
                        conf = jQuery.extend(conf,options)
                        this.box(conf, callback);
                    },
                    error: function (title, content, options, callback) {
                        var conf = {
                            title : title || '错误',
                            content : "<i>"+content+"</i>",
                            color : "#ff0000",
                            colors : ['#ff0000','#aa0000'],
                            colortime: 200,
                            iconSmall : "fa fa-times bounce animated"
                        }
                        conf = jQuery.extend(conf,options)
                        this.box(conf, callback);
                    },
                    bsuccess: function (title, content, options, callback) {
                        var conf = {
                            title : title || '成功',
                            content : "<i>"+content+"</i>",
                            color : "#296191",
                            icon : "fa fa-thumbs-up bounce animated",
                            timeout : 4000
                        }
                        conf = jQuery.extend(conf,options)
                        this.bigbox(conf, callback);
                    },
                    binfo: function (title, content, options, callback) {
                        var conf = {
                            title : title || '提示',
                            content : "<i>"+content+"</i>",
                            color : "#00cc00",
                            icon : "fa fa-info bounce animated",
                            timeout : 4000
                        }
                        conf = jQuery.extend(conf,options)
                        this.bigbox(conf, callback);
                    },
                    bwarn: function (title, content, options, callback) {
                        var conf = {
                            title : title || '警告',
                            content : "<i>"+content+"</i>",
                            color : "#888888",
                            icon : "fa fa-warning bounce animated",
                            timeout : 4000
                        }
                        // 闪烁处理,使用背景切换
                        conf.colors = ["#888888","#555555"]
                        conf.colortime = 200;
                        conf = jQuery.extend(conf,options)
                        this.bigbox(conf, callback);
                    },
                    berror: function (title, content, options, callback) {
                        var conf = {
                            title : title || '错误',
                            content : "<i>"+content+"</i>",
                            color : "#ff0000",
                            colors : ['#ff0000','#aa0000'],
                            colortime: 200,
                            icon : "fa fa-ban bounce animated"
                        }
                        conf = jQuery.extend(conf,options)
                        this.bigbox(conf, callback);
                    }
                },
                modalBox: function (config, callback) {
                    jQuery.SmartMessageBox(config, callback);
                }
            };
        }])
        .factory('LoadingService', function () {
                var loadLayer = window.layer;
                var loading = null;
                var loadCount = 0;

                var loadingModal = null;
                var loadCountModal = 0;

                return {
                    loading: function () {
                        if(loadCountModal==0) {
                            if(loadCount==0) {
                                loading = loadLayer.load(0, 0);
                            }
                        }
                        loadCount ++;
                    },
                    LoadingModal: function (msg) {
                        loadLayer.close(loading);
                        if(loadCountModal==0) {
                            loadingModal = loadLayer.load(msg || '正在拼命加载，请稍后...', 0);
                        }
                        loadCountModal ++;
                    },
                    LoadedModal: function () {
                        loadCountModal --;
                        if(loadCountModal<=0) {
                            loadLayer.close(loadingModal);
                            loadCountModal = 0;
                            if(loadCount>0) {
                                loading = loadLayer.load(0, 0);
                            }
                        }
                    },
                    loaded: function () {
                        loadCount --;
                        if(loadCount<=0) {
                            loadLayer.close(loading);
                            loadCount = 0;
                        }
                    }
                }
        })
        // 时间控件
        .directive('dvLaydate', [function () {
            return {
                restrict: 'A',
                require: '?ngModel',
                scope: {
                    select: '&',
                    config: '&'
                },
                link: function postLink(scope, element, attrs, ngModel) {
                    if (!ngModel) return;
                    var laydate = window.laydate;
                    if (!attrs.id) { // 自动生成id
                        attrs.id = Math.uuid();
                        element.attr('id', attrs.id);
                    }
                    // 设置基本参数
                    var layDateConfig = {
                        elem: '#' + attrs.id + '',
                        choose: function (dateTxt) {
                            // view -> model
                            scope.$apply(function () {
                                ngModel.$setViewValue(dateTxt); // 界面在angularJs以外改变后，通知angularJs更新
                            });

                            scope.select({dateTxt: dateTxt});
                            scope.config() && scope.config()['choose'] && (typeof scope.config()['choose']) == 'function' && scope.config()['choose'](dateTxt);
                        }
                    };

                    // 处理用户阐述参数
                    if (scope.config()) {
                        var customerConfig = scope.config();
                        for (var key in customerConfig) {
                            if (key == 'elem' || key == 'choose') continue;
                            layDateConfig[key] = customerConfig[key];
                        }
                    }
                    // model -> view
//                ngModel.$render = function () { // 告诉angularJS内部模型改变后，怎么更新界面
//                    element.value(ngModel.$viewValue || '');
//                };

                    var laydateObj = laydate(layDateConfig)
                }
            };
        }])
        // 文本编辑器
        .directive('dvKindEditor', ['$debounce', function ($debounce) {
            return {
                restrict: 'A',
                require: '?ngModel',
                scope: {
                    config: '&',
                    vchange: '&'
                },
                link: function postLink(scope, element, attrs, ngModel) {
                    if (!ngModel) return;
                    scope.init = true; // w为了第一次数据显示正常

                    var config = scope.config() || {};
                    config.afterChange = function () {
                        try {
                            // view -> modeld
                            $debounce(function () {
                                scope.$apply(function () {
                                    scope.self = true;
                                    scope.vchange({text: scope.editor.html()});
                                });
                            }, 300);
                        } catch (e) {
                        }
                    };

                    scope.editor = KindEditor.create(element, config);

                    ngModel.$render = function () { // 告诉angularJS内部模型改变后，怎么更新界面
                        if (!scope.self || scope.init)
                            scope.editor.html(ngModel.$viewValue || '');
                        $debounce(function () {
                            scope.self = false;
                        }, 300);

                        scope.init = false;
                    };
                }
            };
        }])
        //防抖处理
        .factory('$debounce', ['$rootScope', '$browser', '$q', '$exceptionHandler',
            function ($rootScope, $browser, $q, $exceptionHandler) {
                var deferreds = {},
                    methods = {},
                    uuid = 0;

                function debounce(fn, delay, invokeApply) {
                    var deferred = $q.defer(),
                        promise = deferred.promise,
                        skipApply = (angular.isDefined(invokeApply) && !invokeApply),
                        timeoutId, cleanup,
                        methodId, bouncing = false;

                    // check we dont have this method already registered
                    angular.forEach(methods, function (value, key) {
                        if (angular.equals(methods[key].fn, fn)) {
                            bouncing = true;
                            methodId = key;
                        }
                    });

                    // not bouncing, then register new instance
                    if (!bouncing) {
                        methodId = uuid++;
                        methods[methodId] = {fn: fn};
                    } else {
                        // clear the old timeout
                        deferreds[methods[methodId].timeoutId].reject('bounced');
                        $browser.defer.cancel(methods[methodId].timeoutId);
                    }

                    var debounced = function () {
                        // actually executing? clean method bank
                        delete methods[methodId];

                        try {
                            deferred.resolve(fn());
                        } catch (e) {
                            deferred.reject(e);
                            $exceptionHandler(e);
                        }

                        if (!skipApply) $rootScope.$apply();
                    };

                    timeoutId = $browser.defer(debounced, delay);

                    // track id with method
                    methods[methodId].timeoutId = timeoutId;

                    cleanup = function (reason) {
                        delete deferreds[promise.$$timeoutId];
                    };

                    promise.$$timeoutId = timeoutId;
                    deferreds[timeoutId] = deferred;
                    promise.then(cleanup, cleanup);

                    return promise;
                }


                // similar to angular's $timeout cancel
                debounce.cancel = function (promise) {
                    if (promise && promise.$$timeoutId in deferreds) {
                        deferreds[promise.$$timeoutId].reject('canceled');
                        return $browser.defer.cancel(promise.$$timeoutId);
                    }
                    return false;
                };

                return debounce;
            }])
            // 文本编辑器上传插件
            .directive('dvKindPlugin', ['$debounce','Config','AuthenticationService', function ($debounce,Config,auth) {
                return {
                    restrict: 'A',
                    require: '?ngModel',
                    template: '<div></div>',
                    scope: {
                        config: '&',
                        vchange: '&'
                    },
                    link: function postLink(scope, elem, attrs, ngModel) {
                       // if (!ngModel) return;
                        scope.init = true; // w为了第一次数据显示正常
                        var config = scope.config() || {};
                        var upload_url=".";
                        if(Config.upload_url!=undefined)
                            config.uploadPath=Config.upload_url
                        config.uploadJson=Config.data_serve_base_url+config.uploadJson+'&upload_url='+upload_url;
                        config.fileManagerJson=Config.data_serve_base_url+config.fileManagerJson+'&upload_url='+upload_url;
                        config.headers={
                            "Authorization" : auth.createAuth()
                        }
                        scope.editor = KindEditor.editor(config);
                        elem.bind('click', function() {
                            scope.editor.loadPlugin(config.pluginType, function() {

                                switch(config.pluginType){
                                    case 'image':scope.editor.plugin.imageDialog({
                                        showLocal:config.showLocal,
                                        showRemote:config.showRemote,
                                        //imageUrl : K('#url1').val(),

                                        clickFn : function(url, title, width, height, border, align) {
                                            scope.editor.hideDialog();
                                            if (!ngModel) return;
                                            scope.$apply(function () {
                                                ngModel.$setViewValue(url); // 界面在angularJs以外改变后，通知angularJs更新
                                            });
                                        }
                                    });
                                        break;
                                    case 'insertfile':scope.editor.plugin.fileDialog({
                                        showLocal:config.showLocal,
                                        showRemote:config.showRemote,
                                        lang:config.themeType,
                                        //fileUrl : K('#fileUrl').val(),
                                        clickFn : function(url, title) {
                                            scope.editor.hideDialog();
                                            if (!ngModel) return;
                                            scope.$apply(function () {
                                                ngModel.$setViewValue(url); // 界面在angularJs以外改变后，通知angularJs更新
                                            });
                                        }
                                    });
                                        break;
                                    case 'filemanager':scope.editor.plugin.filemanagerDialog({
                                        viewType : 'VIEW',
                                        dirName : 'image',
                                        clickFn : function(url, title) {
                                            scope.editor.hideDialog();
                                            if (!ngModel) return;
                                            scope.$apply(function () {
                                                ngModel.$setViewValue(url); // 界面在angularJs以外改变后，通知angularJs更新
                                            });
                                        }
                                    });
                                        break;
                                    case 'multiimage':scope.editor.plugin.multiImageDialog({
                                        showLocal:config.showLocal,
                                        showRemote:config.showRemote,
                                        clickFn : function(urlList) {
                                            /*var div = K('#J_imageView');
                                             div.html('');
                                             K.each(urlList, function(i, data) {
                                             div.append('<img src="' + data.url + '">');
                                             });*/
                                            scope.editor.hideDialog();
                                            if (!ngModel) return;
                                            scope.$apply(function () {
                                                ngModel.$setViewValue(urlList); // 界面在angularJs以外改变后，通知angularJs更新
                                            });
                                        }
                                    });
                                        break;
                                }
                            });
                        });
                    }
                };
            }
            ]
        )
            // 文本编辑器上传插件
            .directive('dvKindUpload', ['$debounce','Config','AuthenticationService', function ($debounce,Config,auth) {
                return {
                    restrict: 'A',
                    require: '?ngModel',
                    scope: {
                        config: '&',
                        vchange: '&'
                    },
                    link: function postLink(scope, elem, attrs,ngModel) {
                        scope.init = true; // w为了第一次数据显示正常
                        var config = scope.config() || {};
                        var upload_url=".";
                        if(Config.upload_url!=undefined)
                            upload_url=Config.upload_url
                        config.url=Config.data_serve_base_url+config.url+'&upload_url='+upload_url
                        config.headers={
                            "Authorization" : auth.createAuth()
                        }
                        scope.uploadbutton = KindEditor.uploadbutton({
                            button : elem[0],
                            fieldName : config.fieldName,
                            url : config.url,
                            afterUpload : function(data) {
                                if (data.error === 0) {
                                    if (!ngModel) return;
                                    scope.$apply(function () {
                                        ngModel.$setViewValue(data.url); // 界面在angularJs以外改变后，通知angularJs更新
                                    })
                                } else {
                                    alert(data.message);
                                }
                            },
                            afterError : function(str) {
                                alert('自定义错误信息: ' + str);
                            }
                        });
                        scope.uploadbutton.fileBox.change(function(e) {
                            scope.uploadbutton.submit();
                        });
                    }
                };
            }
            ]
        )
        ;
});