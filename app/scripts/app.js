/*jshint unused: vars */
define(['angular',
        'app.config',
        'app.init',
        'smartwidgets/jarvis.widget.min',
        'ng/ng.directives',
        'ng/ng.plugins',
        'ng/ng.plugins.directives.dvtree',
        'ng/ng.plugins.directives.dvdataTable',
        'ng/ng.plugins.directives.dvTreeTable',
        'ng/ng.plugins.directives.dvecharts',
        'ng/ng.plugins.directives.dvztree',
        'ng/ng.service',
        'ng/ng.socketio',
        'func/interceptor/http',
        'func/base/config',
        'func/base/service',
        'func/base/base-controllers',
        'func/user/user-ctrl',
        'func/user/login-ctrl',
        'func/function/function',
        'func/role/role',
        'func/demo/demo-ctrl']/*deps*/,

    function (angular)/*invoke*/ {
        'use strict';

        /**
         * @ngdoc overview
         * @name appApp
         * @description
         * # appApp
         *
         * Main module of the application.
         */
        var app = angular.module('app', [
            "ui.bootstrap",
            'app.socket',
            "app.main.directives",
            "app.navigation",
            "app.localize",
            "app.activity",
            "app.smartui",
            'app.plugins',
            'app.modules.demo',
            'app.func.common.service',
            'app.func.interceptor.http',
            'app.func.base.controller',
            'app.func.config',
            'app.func.user',
            'app.func.UserLoginCtrl',
            'app.func.function',
            'app.func.role',
            /*extDeps*/
            'ngRouteResolver',
            /*angJSDeps*/
            'ngCookies',
            'ngResource',
            'ngSanitize',
            'ngRoute',
            'ngAnimate',
            'ngTouch'
        ]);
        app.config(["$controllerProvider","$compileProvider","$filterProvider","$routeProvider", "$provide", "$locationProvider", "$httpProvider","$asyncRouteProvider",
            function ($controllerProvider,$compileProvider,$filterProvider,$routeProvider, $provide, $locationProvider, $httpProvider, $asyncRouteProvider) {
                app.register =
                {
                    controller: $controllerProvider.register,
                    directive: $compileProvider.directive,
                    filter: $filterProvider.register,
                    factory: $provide.factory,
                    service: $provide.service
                };
                $routeProvider
                    .when("/", {
                        redirectTo: "/module/demo/dashboard"
                    })
                    .when('/:page', {
                        templateUrl: function (c) {
                            return 'views/' + c.page + '.html'
                        }
                    })
                    .when('/table/:module/:func', {
                        controller:'DispatcherCtrl',
                        templateUrl: function (c) {
                            return 'scripts/func/base/template/widget-table.html'
                        },
                        access: { requiredLogin: true }
                    })
                    .when('/module/:module/:func', {
                        templateUrl: function (c) {
                            return 'scripts/func/' + c.module + '/template/' + c.func + '.html'
                        },
                        access: { requiredLogin: true }
                    })
                    .when('/async/about', $asyncRouteProvider.route.resolve('About'))
                    .when('/amd/dispatcher/:module/:func', {
                        templateUrl: function (c) {
                            return 'scripts/func/' + c.module + '/template/' + c.func + '.html'
                        },
                        resolve:  {
                            load: ['$q', '$rootScope','$route', function ($q, $rootScope, $route) {
                                console.log($route);
                                var $routeParams = $route.current.params;
                                var dependencies = ['scripts/func/' + $routeParams.module + '/' + $routeParams.func + '-ctrl.js'];

                                var defer = $q.defer();
                                require(dependencies, function () {
                                    defer.resolve();
                                    $rootScope.$apply()
                                });

                                return defer.promise;
                            }]
                        }
                    })
                    .otherwise({
                        redirectTo: "/"
                    });
                $provide.decorator("$log", ["$delegate", function (d) {
                    function c() {
                        c.info.apply(c, arguments)
                    }

                    angular.extend(c, d);
                    return c
                }]);
//                  $locationProvider.html5Mode(true);     // 设置 去掉#号
                $httpProvider.interceptors.push('TokenInterceptor'); // 设置认证拦截器
                $httpProvider.interceptors.push('HttpErrorInterceptor'); // 设置错误拦截器
            }]);
        app.run(["$rootScope", "settings", "Config", "$location", "AuthenticationService", 'localize', 'UserFuncService',
            function ($rootScope, settings, config, $location, AuthServ, localize, UserFuncService) {
                settings.currentLang = settings.languages.Chinese;

                var currentLang = navigator.language;   //判断除IE外其他浏览器使用语言
                if (!currentLang) {//判断IE浏览器使用语言
                    currentLang = navigator.browserLanguage;
                }
                console.log("当前语言环境：" + currentLang);

                // 监听用户操作，验证权限
                $rootScope.$on("$routeChangeStart", function (event, nextRoute, currentRoute) {
                    if (nextRoute.access && nextRoute.access.requiredLogin && !AuthServ.isLogin) {
                        location.href = "login.html"
                    }
                });

            }])
        ;
        return app;
    });
