/*
 * Unminified JS files are included once purchased.
 */

$.root_ = $("body");
$.intervalArr = [];
var calc_navbar_height = function () {
        var a = null;
        if ($("#header").length) {
            a = $("#header").height()
        }
        if (a === null) {
            a = $('<div id="header"></div>').height()
        }
        if (a === null) {
            return 49
        }
        return a
    },
    navbar_height = calc_navbar_height,
    shortcut_dropdown = $("#shortcut"),
    bread_crumb = $("#ribbon ol.breadcrumb"),
    topmenu = true,
    thisDevice = null,
    jsArray = {},
    ismobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
if (!ismobile) {
    $.root_.addClass("desktop-detected");
    device = "desktop"
} else {
    $.root_.addClass("mobile-detected");
    device = "mobile";
    if (fastClick) {
        $.root_.addClass("needsclick");
        FastClick.attach(document.body)
    }
}
if ($("body").hasClass("menu-on-top") || topmenu==true || localStorage.getItem("sm-setmenu") == "top") {
    topmenu = true;
    $("body").addClass("menu-on-top")
}
jQuery(document).ready(function () {
    if ($("[rel=tooltip]").length) {
        $("[rel=tooltip]").tooltip()
    }
    $(document).mouseup(function (a) {
        if (!$(".ajax-dropdown").is(a.target) && $(".ajax-dropdown").has(a.target).length === 0) {
            $(".ajax-dropdown").fadeOut(150);
            $(".ajax-dropdown").prev().removeClass("active")
        }
    });
    $("button[data-btn-loading]").on("click", function () {
        var a = $(this);
        a.button("loading");
        setTimeout(function () {
            a.button("reset")
        }, 3000)
    })
});
// 定义 resize事件处理
(function (jquery, _this, c) {
    var array = jquery([]),
        resizeConfig = jquery.resize = jquery.extend(jquery.resize, {}),
        j,
        setTimeout = "setTimeout",
        resize = "resize",
        resizeEvent = resize + "-special-event",
        delay = "delay",
        throttleWindow = "throttleWindow";
    resizeConfig[delay] = throttle_delay;
    resizeConfig[throttleWindow] = true;
    jquery.event.special[resize] = {
        setup: function () {
            if (!resizeConfig[throttleWindow] && this[setTimeout]) {
                return false
            }
            var m = jquery(this);
            array = array.add(m);
            try {
                jquery.data(this, resizeEvent, {w: m.width(), h: m.height()})
            } catch (n) {
                jquery.data(this, resizeEvent, {w: m.width, h: m.height})
            }
            if (array.length === 1) {
                h()
            }
        },
        teardown: function () {
            if (!resizeConfig[throttleWindow] && this[setTimeout]) {
                return false
            }
            var m = jquery(this);
            array = array.not(m);
            m.removeData(resizeEvent);
            if (!array.length) {
                clearTimeout(j)
            }
        },
        add: function (m) {
            if (!resizeConfig[throttleWindow] && this[setTimeout]) {
                return false
            }
            var o;

            function n(t, p, q) {
                var r = jquery(this), s = jquery.data(this, resizeEvent);
                s.w = p !== c ? p : r.width();
                s.h = q !== c ? q : r.height();
                o.apply(this, arguments)
            }

            if (jquery.isFunction(m)) {
                o = m;
                return n
            } else {
                o = m.handler;
                m.handler = n
            }
        }
    };
    function h() {
        j = _this[setTimeout](function () {
            array.each(function () {
                var n;
                var m;
                var o = jquery(this), p = jquery.data(this, resizeEvent);
                try {
                    n = o.width()
                } catch (q) {
                    n = o.width
                }
                try {
                    m = o.height()
                } catch (q) {
                    m = o.height
                }
                if (n !== p.w || m !== p.h) {
                    o.trigger(resize, [p.w = n, p.h = m])
                }
            });
            h()
        }, resizeConfig[delay])
    }
})(jQuery, this);

$("#main").resize(function () {
    check_if_mobile_width()
});
function check_if_mobile_width() {
    if ($(window).width() < 979) {
        $.root_.addClass("mobile-view-activated");
        $.root_.removeClass("minified")
    } else {
        if ($.root_.hasClass("mobile-view-activated")) {
            $.root_.removeClass("mobile-view-activated")
        }
    }
}
var ie = (function () {
    var c, a = 3, d = document.createElement("div"), b = d.getElementsByTagName("i");
    while (d.innerHTML = "<!--[if gt IE " + (++a) + "]><i></i><![endif]-->", b[0]) {
    }
    return a > 4 ? a : c;
}());

$.fn.extend({jarvismenu: function (a) {
    var d = {accordion: "true", speed: 200, closedSign: "[+]", openedSign: "[-]"};
    var b = $.extend(d, a);
    var c = $(this);
    c.find("li[init!=init]").each(function () {
        if ($(this).find("ul").size() !== 0) {
            $(this).find("a:first").append("<b class='collapse-sign'>" + b.closedSign + "</b>");
            if ($(this).find("a:first").attr("href") == "#" || $(this).find("a:first").attr("href") == "") {
                $(this).find("a:first").attr("href", "javascript:void(0)");
                $(this).find("a:first").click(function () {
                    return false
                })
            }
        }
    });
    c.find("li[init!=init].active").each(function () {
        $(this).parents("ul").slideDown(b.speed);
        $(this).parents("ul").parent("li").find("b:first").html(b.openedSign);
        $(this).parents("ul").parent("li").addClass("open")
    });

    c.find("li[init!=init] a").click(function () {
        if ($(this).parent().find("ul").size() !== 0) {
            if (b.accordion) {
                if (!$(this).parent().find("ul").is(":visible")) {
                    parents = $(this).parent().parents("ul");
                    visible = c.find("ul:visible");
                    visible.each(function (e) {
                        var f = true;
                        parents.each(function (g) {
                            if (parents[g] == visible[e]) {
                                f = false;
                                return false
                            }
                        });
                        if (f) {
                            if ($(this).parent().find("ul") != visible[e]) {
                                $(visible[e]).slideUp(b.speed, function () {
                                    $(this).parent("li").find("b:first").html(b.closedSign);
                                    $(this).parent("li").removeClass("open")
                                })
                            }
                        }
                    })
                }
            }
            if ($(this).parent().find("ul:first").is(":visible") && !$(this).parent().find("ul:first").hasClass("active")) {
                $(this).parent().find("ul:first").slideUp(b.speed, function () {
                    $(this).parent("li").removeClass("open");
                    $(this).parent("li").find("b:first").delay(b.speed).html(b.closedSign)
                })
            } else {
                $(this).parent().find("ul:first").slideDown(b.speed, function () {
                    $(this).parent("li").addClass("open");
                    $(this).parent("li").find("b:first").delay(b.speed).html(b.openedSign)
                })
            }
        }
    })
    c.find("li[init!=init]").attr("init", "init")
}});

jQuery.fn.doesExist = function () {
    return jQuery(this).length > 0
};

$("body").on("click", function (a) {
    $('[rel="popover"]').each(function () {
        if (!$(this).is(a.target) && $(this).has(a.target).length === 0 && $(".popover").has(a.target).length === 0) {
            $(this).popover("hide")
        }
    })
});