/**
 * Created by three on 14-7-15.
 */

define(['angular','ng/ng.plugins','smartwidgets/jarvis.widget.min',
    'func/user/login-ctrl',
    'func/base/config',
    'func/interceptor/http',
    'func/base/base-controllers']/*deps*/, function (angular)/*invoke*/ {
    'use strict';

    return angular.module('appLogin', [
        'app.func.UserLoginCtrl',
        'app.func.config',
        'app.func.interceptor.http',
        'app.plugins',
        'app.func.base.controller',
        /*angJSDeps*/
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute'
    ], function () {

    })
        .config(["$routeProvider", "$provide", "$locationProvider", "$httpProvider",
        function ($routeProvider, $provide, $locationProvider, $httpProvider) {
            $httpProvider.interceptors.push('TokenInterceptor'); // 设置认证拦截器
            $httpProvider.interceptors.push('HttpErrorInterceptor'); // 设置错误拦截器
        }])

    ;
});
