/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Controller: FuncUserLoginCtrl', function () {

    // load the controller's module
    beforeEach(module('appApp.controllers.FuncUserLoginCtrl'));

    var FuncUserLoginCtrl,
      scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
      scope = $rootScope.$new();
      FuncUserLoginCtrl = $controller('FuncUserLoginCtrl', {
        $scope: scope
      });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
      expect(scope.awesomeThings.length).toBe(3);
    });
  });
});
