var tests = [];
for (var file in window.__karma__.files) {
  if (window.__karma__.files.hasOwnProperty(file)) {
    // Removed "Spec" naming from files
    if (/Spec\.js$/.test(file)) {
      tests.push(file);
    }
  }
}

requirejs.config({
    // Karma serves files from '/base'
    baseUrl: '/base/app/scripts',

    paths: {
        'jquery': '../bower_components/jquery/dist/jquery.min',
        'jquery-easing':'plugin/jquery-easing/jquery.easing.1.3',
        'jquery-ui':'../bower_components/jquery-ui/jquery-ui.min',
        'bootstrap': '../bower_components/bootstrap/dist/js/bootstrap.min',
        'angular': '../bower_components/angular/angular.min',
        'ui-bootstrap-custom-tpls': 'plugin/angular/ui-bootstrap-custom-tpls-0.11.0',
        'angular-route': '../bower_components/angular-route/angular-route.min',
        'angular-cookies': '../bower_components/angular-cookies/angular-cookies.min',
        'angular-sanitize': '../bower_components/angular-sanitize/angular-sanitize.min',
        'angular-resource': '../bower_components/angular-resource/angular-resource.min',
        'angular-animate': '../bower_components/angular-animate/angular-animate.min',
        'angular-touch': '../bower_components/angular-touch/angular-touch.min',
        'angular-mocks': '../bower_components/angular-mocks/angular-mocks',
        'datatables/bootstrap': 'plugin/datatables/dataTables.bootstrap.min',
        'datatables/jquery': 'plugin/datatables/jquery.dataTables.min',
        'datatables/colVis': 'plugin/datatables/dataTables.colVis.min',
        'datatables/tools': 'plugin/datatables/dataTables.tableTools.min',
        'datatables': 'plugin/datatable-responsive/datatables.responsive.min',
        'app': 'app',
        'app.init':'app.init'
    },

    shim: {
        'angular' : {'exports' : 'angular'},
        'angular-route': ['angular'],
        'angular-cookies': ['angular'],
        'angular-sanitize': ['angular'],
        'angular-resource': ['angular'],
        'angular-animate': ['angular'],
        'angular-touch': ['angular'],
        'angular-mocks': {
          deps:['angular'],
          'exports':'angular.mock'
        }
    },

    // ask Require.js to load these files (all our tests)
    deps: tests,

    // start test run, once Require.js is done
    callback: window.__karma__.start
});
